<?php
/**
 * Plugin Name: CSV to Page
 * Plugin URI: http://www.terrifickid.net
 * Description: Create pages from CSV Files
 * Version: 1.0.1
 * Author: msplint
 * Author URI: http://www.terrifickid.net

 
            _ _       _            
          | (_)     | |           
 ___ _ __ | |_ _ __ | |_ ___ _ __ 
/ __| '_ \| | | '_ \| __/ _ \ '__|
\__ \ |_) | | | | | | ||  __/ |   
|___/ .__/|_|_|_| |_|\__\___|_|   
    | |                           
    |_|   
    
  */
require(plugin_dir_path( __FILE__ ).'/helper.php');
require( plugin_dir_path( __FILE__ ) . '/acf/acf.php' );
add_filter('acf/settings/show_admin', '__return_false');
add_filter('acf/settings/dir', 'my_acf_settings_dir');
add_filter('acf/settings/path', 'my_acf_settings_path');
add_filter( 'gettext', 'csv_change_text', 10, 2 );
acf_add_options_page(array(
		'page_title' 	=> 'CSV To Page',
		'menu_title'	=> 'CsvToPage',
		'menu_slug' 	=> 'csvtopage',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position' => 100,
		'icon_url' => 'dashicons-exerpt-view',
	));


add_action('acf/init', 'my_acf_add_local_field_groups');
add_action('acf/save_post', 'ctp_csvToPage', 20);




    
