<?php

 
function my_acf_settings_path( $path ) {
 $path = plugins_url( '/csvToPage', dirname(__FILE__) ) . '/acf/';
 return $path;
}

function my_acf_settings_dir( $dir ) {
  $dir = plugins_url( '/csvToPage', dirname(__FILE__) ) . '/acf/';
  return $dir;
}




function csv_change_text( $translation, $text ) {
	
if($_REQUEST['page'] == 'csvtopage'){	
if ( $text == 'Options Updated') return 'CsvToPage Executed, Pages Created';
if ( $text == 'Publish' ) return 'Execute';
if ( $text == 'Update' ) return 'Execute CSV to Page';
}
return $translation;

}



function _combine_array(&$row, $key, $header) {
  $row = array_combine($header, $row);
}

function ctp_clear($string){
	$string = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($string))))));
	return $string;
}
function ctp_csvToPage() {
	$screen = get_current_screen();
	if ($screen->id == "toplevel_page_csvtopage") {
		$csv_file = get_attached_file(get_field('ctp_csv_file', 'option'), false);
		$pages = array();
		
		
		
		ini_set('auto_detect_line_endings',TRUE);
			$handle = fopen($csv_file,'r');
			while ( ($data = fgetcsv($handle) ) !== FALSE ) array_push($pages, $data);
			ini_set('auto_detect_line_endings',FALSE);
			$header = array_shift($pages);
			array_walk($pages, '_combine_array', $header);
		
		    	
		    	$content = get_field('ctp_content', 'option');
				$title = get_field('ctp_title', 'option');
				$permalink = get_field('ctp_permalink', 'option');
				$seotitle = get_field('ctp_seo_title', 'option');
				$keyphrase = get_field('ctp_focus_keyphrase', 'option');
				$meta = get_field('ctp_seo_meta_description', 'option');
				
				
				
			
	foreach($pages as $page){
		
		if(!is_array($page)) break;
		
		$my_title = $title;
		$my_content = $content;
		$my_permalink = $permalink;
		$my_seotitle = $seotitle;
		$my_keyphrase = $keyphrase;
		$my_meta = $meta;
	 
		
		foreach($page as $find => $replace){
			$find = ctp_clear($find);
			$find = '['.$find.']';
			
			$my_content = str_replace($find, $replace, $my_content);
			$my_title = str_replace($find, $replace, $my_title);
			$my_permalink = str_replace($find, $replace, $my_permalink);
			$my_seotitle = str_replace($find, $replace, $my_seotitle);
			$my_keyphrase = str_replace($find, $replace, $my_keyphrase);
			$my_meta = str_replace($find, $replace, $my_meta);
		}
	
	
		
		
			// Create post object
			$my_post = array(
			  'post_title'    => $my_title,
			  'post_content'  => $my_content,
			  'post_name' => sanitize_title($my_permalink),
			  'post_status'   => 'draft',
			  'post_type' => 'page'
			);
		
		
			
				//Insert the post into the database
				$ID = wp_insert_post( $my_post );
				//Set Yoast SEO Fields
				 update_post_meta( $ID, '_yoast_wpseo_title', $my_seotitle );
				 update_post_meta( $ID, '_yoast_wpseo_metakeywords', strtolower($my_keyphrase) );
				 update_post_meta( $ID, '_yoast_wpseo_metadesc', $my_meta );
				//print_r($tag);
	}
	
	
	
	
		
		
		
	
	
	}
}




function my_acf_add_local_field_groups() {
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_59e3bec2a0cf6',
	'title' => 'Generator Settings',
	'fields' => array (
		array (
			'key' => 'field_59e3c4bede00d',
			'label' => 'CSV File',
			'name' => 'ctp_csv_file',
			'type' => 'file',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'id',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_59e3c4e29c500',
			'label' => 'Title',
			'name' => 'ctp_title',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59e6af53dd7c4',
			'label' => 'Permalink',
			'name' => 'ctp_permalink',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59e3c34d6afd0',
			'label' => 'Content',
			'name' => 'ctp_content',
			'type' => 'wysiwyg',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'csvtopage',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_59e7e45a1d456',
	'title' => 'Yoast SEO Settings',
	'fields' => array (
		array (
			'key' => 'field_59e7e46193ca9',
			'label' => 'SEO Title',
			'name' => 'ctp_seo_title',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59e7e47504476',
			'label' => 'SEO Meta Keywords',
			'name' => 'ctp_focus_keyphrase',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59e7e4e0b8f23',
			'label' => 'SEO Meta Description',
			'name' => 'ctp_seo_meta_description',
			'type' => 'textarea',
			'value' => NULL,
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 5,
			'new_lines' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'csvtopage',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
}

