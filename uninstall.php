<?php
	// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

delete_option( 'option_ctp_csv_file' );
delete_option( '_option_ctp_csv_file' );
	
delete_option( 'option_ctp_title' );
delete_option( '_option_ctp_title' );
	